# NixOS I3 Config
This section explains how to set up this configuration.
## Getting Started
### Installing Nixos
The installation of NixOS is largely straight forward. There are a few points to note:
* you'll need a [iso image](https://nixos.org/download/) and a program to load it to your device. You can either use:
  * [balena-etcher](https://etcher.balena.io/) As far as I know, they have switched to offering the program in the package sources or building it directly from a zip file.
  * dd
* There are now several images. One with Gnome preinstalled. The other with KDE. Since you can later use [NixOS] standard commands to throw down everything that is no longer needed anyway, it doesn't matter whether you use an installer with or without a graphical interface. I recommend the installer with gnome.
### Getting the Resources
In this section, we'll begin by getting our resources from [GitLab](https://gitlab.com/) and then go step by step to make changes. You'll be able to adjust various aspects of your system like what software it has and how it behaves. Let's take it one step at a time to make your [NixOS](https://nixos.org/) work just the way you like it.

#### [Clone GitLab Repository](https://gitlab.com/nixos8471109/nixos_i3_setup)
To clone a GitLab Repository, ensure you have [Git](https://git-scm.com/) installed on your system. By default, Git may not be available. We can use a shell instance of Git using the following command:
```sh
nix-shell -p git
```
Now that we have git, execute the following command to clone the snippet:
```sh
git clone https://gitlab.com/nixos8471109/nixos_i3_setup.git > ~/dotfiles
```
This will create a directory named 'dotfiles' in your home.`cd` into 'dotfiles' you will find the content of this Repository there. With this, you have successfully cloned the desired configuration file.
### Adapting the files to your configuration:
First of all, the boot settings in the `configuration.nix` here must be changed. These can usually be found relatively high up in `/etc/nixos/configuration.nix`. It could look like this:
```nix
  # paste your boot config here...
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    };
```
The boot settings found in this file must be placed at the position in `configuration.nix`, which was found in this repository.  
Next, the user configurations of `/etc/nixos/configuration.nix` must be copied and pasted here.
```nix
users={
      defaultUserShell = pkgs.nushell;
      users.azrael = {
	    isNormalUser = true;
	    description = "azrael";
	    extraGroups = [ "networkmanager" "wheel" "docker" ];
      };
	  }; # users
```
You'll need to change `users.azrael` and the String in `description = "azrael"` through
your actual username from `/etc/nixos/configuration.nix`

At least, you'll need to change the timezones in `/configuration.nix`
```nix
i18n = {
    defaultLocale = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
      LC_CTYPE="en_US.utf8"; # required by dmenu don't change this
    };
  };
```
if you like, you can also customize the other files in this folder.

### Symlinking everything together
Of course you could move all the files here to the appropriate places and they would still do their job. Leaving them all in the same place has the huge advantage of not having to search for everything all the time. first you have to create the appropriate folders:
```sh
mkdir -p ~/.config/alacritty/
mkdir -p ~/.config/i3/
mkdir -p ~/.config/i3status/
mkdir -p ~/.config/nushell/
```
#### Corresponding hard links must be created for all files. 
A brief overview of Linux hard 
links: In Linux, allocated memory and the corresponding hard links are distinct 
concepts. For memory to be considered allocated, it must have at least one hard link 
pointing to it. Certain programs may explicitly look for these links using specific 
identifiers. For instance, `HOME/.config/nushell/config.nu` represents the hard link 
to the [Nushell](https://www.nushell.sh/) configuration file. You can create 
multiple hard links to the same file. To release the allocated memory, you must 
delete all of these hard links.
Now lets link everything together:
```sh
sudo ln -s ~/dotfiles/configuration.nix /etc/nixos/configuration.nix
ln -s ~/dotfiles/i3statusconfig ~/.config/i3status/config
ln -s ~/dotfiles/i3config ~/.config/i3/config
ln -s ~/dotfiles/nushell/config.nu ~/.config/nushell/config.nu
ln -s ~/dotfiles/nushell/env.nu ~/.config/nushell/env.nu
```
This step can sometimes lead to errors. If in doubt, you must rename or delete files that already exist in the corresponding target paths.
#### Confirming the links:
```sh
ls -a /etc/nixos/
```
should respond to something similar like:
```sh
/etc/nixos/configuration.nix,symlink,39 B,2024-02-26 10:27:22.627525388 +01:00
```
The other files should also be linked accordingly.
