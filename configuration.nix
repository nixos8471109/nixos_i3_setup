{ config, pkgs, ... }:
let
  nixvim = import (builtins.fetchGit{url = "https://github.com/nix-community/nixvim";});
in
let 
  rosDocker=import (builtins.fetchGit{url="https://gitlab.com/docker5348/ros2_main_dev_container";});
in
{
  imports = [
    /etc/nixos/hardware-configuration.nix
  ];
  nix={
    gc = {
      automatic = true;
      dates = "daily";
    };
    settings={
      auto-optimise-store=true;
      experimental-features = [ "nix-command" "flakes" ];
      };
  };

  # paste your boot config here...
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    };

  virtualisation.docker.enable = true;
  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [];
      allowedUDPPorts = [];
    };
    hostName = "Azrael";
    networkmanager.enable = true;
  };

  # edit as per your location and timezone
  time.timeZone = "Europe/Berlin";

  i18n = {
    defaultLocale = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
      LC_CTYPE="en_US.utf8"; # required by dmenu don't change this
    };
  };

  
	services = {
    displayManager.defaultSession= "xfce+i3";
    libinput.enable=true;
	  printing.enable = true;
    signald={
      enable=true;
      user="azrael";
    };
	  xserver = {
	    xkb={
	      layout = "eu";
	      variant = "";
	      };
	    enable = true;
	    windowManager.i3 = {
	      enable = true;
	      };
      desktopManager = {
		    xterm.enable = true;
		    xfce = {
		      enable = true;
		      noDesktop = true;
		      enableXfwm = false;
		    };
	    };
	    displayManager = {
		    lightdm.enable = true;
        sessionCommands="xhost si:localuser:root";
	    };
	  }; # xserver
	    gvfs.enable = true;
	    gnome.gnome-keyring.enable = true;
	    blueman.enable = true;
	    pipewire = {
	      enable = true;
	      alsa = {
		      enable = true;
		      support32Bit = true;
	      };
	      pulse.enable = true;
	    };
	  };
	  nixpkgs = {
	    config = {
	      allowUnfree = true;
	      pulseaudio = true;
	    };
	  };

	  users={
      defaultUserShell = pkgs.nushell;
      users.azrael = {
	    isNormalUser = true;
	    description = "azrael";
	    extraGroups = [ "networkmanager" "wheel" "docker" ];
      };
	  }; # users

	environment = {
    localBinInPath=true;
    variables= {
	    SRC_ENDPOINT = "https://sourcegraph.example.com";
	    SRC_SRC_ACCESS_TOKEN="'cat ${config.users.users.azrael.home}/sourcegraphToken'";
	  };
    shellAliases = {
	    sudo="sudo ";
	    nixos_list_generations="sudo nix-env --list-generations --profile /nix/var/nix/profiles/system";
	    nixosRebuild="sudo nixos-rebuild switch";
	    dockerRmContainters="docker ps -a | awk '{print $NF}' | while read -r container_name; do docker rm $container_name; done";
	    dockerRmImages="docker image ls --quiet --no-trunc | while read -r image_id; do docker image rm $image_id; done";
      };
	  systemPackages = with pkgs; [
      alacritty
      alacritty-theme
      bat
      bluez
      bluez-tools
	    cargo
      catimg
	    clang
      curl
      cmake
      cytoscape
	    dmenu
	    docker
      fd
	    firefox
	    fuse
	    gcc 
      glab
      git
      gimp
	    gnome.gnome-keyring
	    gr-framework
      htop
	    i3
	    i3status
 	    inkscape
	    jabref
      jql
	    julia
      just
	    kate
	    lazygit
	    libclang
      lld_18
      mc
	    nerdfonts
      networkmanagerapplet
	    neovim
	    nitrogen
      nixfmt-rfc-style
      nodejs_22
	    pasystray
	    obsidian
	    openjdk
	    openscad-unstable
	    picom
	    pkg-config
	    polkit_gnome
	    pulseaudioFull
	    python312
      python313
	    python312Packages.polars
      python312Packages.numpy
      python312Packages.scipy
      python312Packages.pip
      python312Packages.ortools
      ravedude
      ripgrep
      rofi
	    roboto
	    rustc
	    rustup
	    steam
      signald
      signal-desktop-beta
	    tectonic
      tree
      tree-sitter
	    thunderbird
      typst
      typstfmt
      typst-lsp
	    ucs-fonts
      unetbootin
	    unrar
	    unzip
      uv
	    vcstool
	    vim
	    vscode
	    wget
	    xclip
	    xorg.xhost
	    xorg.libX11
	    x11docker
	    zip
	    (vscode-with-extensions.override {
        vscodeExtensions = with vscode-extensions; [
          bbenoist.nix
          ms-azuretools.vscode-docker
          ms-python.python
          ms-toolsai.jupyter
          ms-toolsai.jupyter-keymap
          ms-toolsai.jupyter-renderers
          ms-toolsai.vscode-jupyter-slideshow
          ms-toolsai.vscode-jupyter-cell-tags
          ms-vscode-remote.remote-ssh
          serayuzgur.crates
          tamasfe.even-better-toml
          ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [{
            name = "remote-ssh-edit";
            publisher = "ms-vscode-remote";
            version = "0.47.2";
            sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
          }];
      })
	  ];
  }; # environment

	programs = {
	  dconf.enable = true;
	  firefox={
	    enable=true;

	  };
	  git.enable = true;
	  neovim = {
	    enable = true;
	    defaultEditor = true;
	  };
    ssh.startAgent=true;
	  steam = {
	    enable = true;
	    remotePlay.openFirewall = true;
	    dedicatedServer.openFirewall = true;
	  };
    thunar.enable = true;
	};# programs

	security = {
    polkit.enable = true;
	  rtkit.enable = true;
  };

  systemd={
    user.services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart =
          "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  }; # systemd
  
  hardware.bluetooth.enable = true;

  # Don't touch this
  system={
    activationScripts={
    script.text =
    ''
       mkdir -p ${config.users.users.azrael.home}/{ros2_ws,microros_ws,ros2_debug}/src
    '';
    };
    stateVersion = "unstable";
    };
}

